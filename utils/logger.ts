import config from 'config';
import winston from 'winston';
import WinstonCloudWatch from 'winston-cloudwatch';

const { createLogger, format, transports } = winston;
const { combine, timestamp, printf, colorize } = format;

const logFormat = printf(
  info => `[${info.timestamp}] [${info.level}]: ${info.message}`
);

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
};

//
// Based on environment log the information
//
const getLogLevel = () => {
  const env = config.get('env') || '';
  if(env !== 'production') {
    return 'silly';
  }

  return 'verbose';
}

const logger = createLogger({
  levels,
  format: combine(
    colorize(),
    timestamp({ format: 'MM-DD-YY HH:mm:ss' }),
    logFormat
  ),
  exitOnError: false,
  transports: [
    new transports.Console({ level : getLogLevel() })
  ]
});

if (config.get("aws.logToCloudWatch")) {
  logger.add(
    new WinstonCloudWatch({
      awsAccessKeyId: config.get("aws.accessKeyId"),
      awsSecretKey: config.get("aws.secretAccessKey"),
      awsRegion: config.get("aws.region"),
      name: 'sweet-tech-ws-api',
      logGroupName: config.get("aws.cloudWatchLogGroupName"),
      logStreamName: config.get("aws.cloudWatchLogStreamName"),
      level: 'silly'
    })
  );
}

// @TODO: setup stream for morgan logger, for API logs

export default logger;
