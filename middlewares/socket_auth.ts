import { IncomingMessage } from "http";

const socketAuthentication = (request: IncomingMessage) => {
    return new Promise((resolve, reject) => {
        if (!request.headers.authorization) {
            return reject(new Error('Authorization not found'));
        }
        
        // @TODO: after authorization resolve user data 
        // here we are returning dummy data
        const userData = {};
       return resolve(userData);
    });
};

export default socketAuthentication;