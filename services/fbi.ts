import fetch from "node-fetch";
import config from 'config';
import logger from "../utils/logger";

interface FbiInterface {
  page: number
  fieldOffices: string;
  sex: string;
}

export default class Fbi {
  apiBaseUrl: string;
  page: number;
  fieldOffices: string;
  sex: string;

  constructor(page = 1, fieldOffices = "", sex = "") {
    logger.info(`page=${page}, fieldOffices=${fieldOffices}, sex=${sex}`)
    this.apiBaseUrl = config.get('fbiBaseApiUrl');
    this.page = page;
    this.fieldOffices = fieldOffices;
    this.sex = sex;
  }

  async getWantedList(): Promise<any> {
    try {
      const url = `${this.apiBaseUrl}/wanted/v1/list?page=${this.page}&field_offices=${this.fieldOffices}&sex=${this.sex}`;
      logger.debug("Fbi api url " + url);
      // @TODO: return following if you want to pipe a stream
      // return fetch(url);
      return (await fetch(url)).json();
    } catch (e) {
      logger.error(e)
      return { "status": 500, "data": e, "msg": "Internal Server Error" }
    }
  }

  async getPerson(personId: string): Promise<any> {
    try {
      const url = `${this.apiBaseUrl}/@wanted-person/${personId}`;
      logger.debug("Fbi api url " + url);
      // set this person's data in redis as cache
      // await io_redis.set("person" + this.id, JSON.stringify(person))
      return (await fetch(url)).json();
    } catch (e) {
      //Log process start
      logger.error(e)
      return { "status": 500, "data": e, "msg": "Internal Server Error" }
    }
  }
}