FROM node:13.13.0-alpine AS alpine

#  ref: https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

WORKDIR /app

COPY package*.json ./

# RUN npm install --production
RUN npm install
COPY . .
COPY .env .env

# this is for development
EXPOSE 8080
CMD ["npm", "start"]

# @TODO: for production we can use this command
# CMD ["npm", "run", "prod"]