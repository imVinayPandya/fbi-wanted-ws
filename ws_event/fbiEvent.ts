import Fbi from "../services/fbi";

// @TODO:  add proper interface for type check
export const getWantedList = async (data: any) => {
    const fbi = new Fbi(data.page, data.fieldOffices, data.sex);
    const list = await fbi.getWantedList();
    return list;
}

export const getPerson = async (personId: any) => {
    const fbi = new Fbi();
    const person = await fbi.getPerson(personId);
    return person;
}

const handleEvent = async (type: string, data: any) => {
    switch (type) {
        case 'get_persons':
            return JSON.stringify(await getWantedList(data));
        case 'person_details':
            return JSON.stringify(await getPerson(data?.personId));

        default:
            return '';
    }
}

export default handleEvent;