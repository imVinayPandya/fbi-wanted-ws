import fs from 'fs';
import path from 'path';
import express from 'express';
import jwt from 'jsonwebtoken';
import { v4 as uuidv4 } from 'uuid';

const router = express.Router();
const privateKeyPath = path.join(__dirname, '..', '.cert', 'jwt_private');
const privateKey = fs.readFileSync(privateKeyPath);

/* GET users listing. */
router.get('/', async (_req, res) => {
  res.send('respond with a resource');
});

router.post('/login', async (req, res) => {
  const { username, type, password } = req.body;

  // @TODO: do API validation and
  // fetch user details from DB and validate credentials

  // generate token
  const token = jwt.sign({
    userId: uuidv4(),
    iss: 'sweet_tech',
    role: 'ST_USER'
  }, privateKey, { algorithm: 'RS256', expiresIn: '24h' });

  const resObj = {
    success: true,
    data: {
      token,
      user: {
          firstName: 'dummy_firstName',
          lastName: 'dummy_lastName',
      }
    },
    message: 'Login successful'
  };

  return res.status(200).send(resObj);
});

export default router;
