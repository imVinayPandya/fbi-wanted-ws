import express from 'express';
const router = express.Router();

import userRouter from './user';

/* GET home page. */
router.get('/', function (req, res, next) {
  res.status(200).send("Welcome to the hell!");
});

router.use('/user', userRouter);

export default router;
