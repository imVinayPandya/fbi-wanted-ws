# fbi-wanted-ws

See TASK.md for the code challenge description.

# Run the Node app

`npm start`

Note: it will start with **nodemon**

# Start the docker container

`chmod 0755 ./start.sh`

`./start.sh`

# Run docker-compose

`docker-compose up`

# Run test cases

`npm test`


# Things to improve

- Use Kubernetes
- We can change log level dynamically in production ref: https://stackoverflow.com/a/59855599/2036977
- Use node cluster for running multiple node app
- We can use load balancer, such as nginx or ha-proxy
- Redis for caching api response
- DOS or Multiple request Prevention with redis
- setup CI-CD
- We can also set SSL and gzip compression in nginx setup
- Better process management we can use PM2
- Use **Joi** or **AJV** for payload/request validation
- Avoid putting .env and .cert/ directory in git repo
- Mock api response while running test cases, to save API calls
- Use proper Interface or typechecking in code
- Use code coverage with jest, ref: https://stackoverflow.com/a/54173575/2036977
- Use eslint + prettier for code linting (air-bnb or google standard linting)
- Create custom error class to do proper error handling