# Introduction

The goal of the code challenge is to create an asynchronous version of an existing REST API, using websockets.

Please do not spend more than 3 hours working on this task.

There are many tasks listed below and we do NOT expect you to finish implementing all of them within 3 hours.
You can choose to skip or adjust the scope of a task in a meaningful way, if you think it would take too much time to implement.
Please write a comment on your rational, and make a few notes on how you might solve it, given you had more time to finish up.

We are expecting an application that we can run with one or more integration tests demonstrating some of the features you have implemented.

The application does not have to be feature complete.

All tasks will be discussed in the post challenge interview. You can present the code you have written and we can discuss the parts that you may have choosen to skip or limit,
due to time constraints or other reasons.

We are just as much interested in what you are thinking as the code you have written.

The application you create must be a Node.js application and it should be written in Typescript.

This Node.js project is set up with:
  - Typescript (https://www.typescriptlang.org/)
  - Jest for testing (https://www.npmjs.com/package/jest)
  - Dependencies that might be useful:
    - express (https://www.npmjs.com/package/express)
    - ws (https://www.npmjs.com/package/ws)
    - dotenv (https://www.npmjs.com/package/dotenv)
    - node-fetch (https://www.npmjs.com/package/node-fetch)

You can choose to use them or replace them with any other packages you prefer.

Your project should contain a README with instructions on how we can:

- install
- build
- start
- run integration test


# Prerequisite

This challenge will touch on some of the subjects you would see in the job:

- Node.js
- Typescript
- REST
- Websockets
- Docker
- Kubernetes

You will probably need the following installed before you start the task:

- NodeJS. See: https://nodejs.org/en/download/
- Docker. See: https://docs.docker.com/engine/install/
- Kubernetes command-line tool. See: https://kubernetes.io/docs/tasks/tools/
- Minikube: for running a local Kubernetes cluster. See: https://minikube.sigs.k8s.io/docs/

If you want to use other tools in addition or instead, that's fine, just make a note of it in the README.

NOTE: You don't need to count installation and configuration time as part of the task duration.

# 1 Websocket API

You will be working with the FBI Wanted REST API.

Official API description: https://www.fbi.gov/wanted/api 

The REST API provides a single endpoint with pagination and search options.

Example:

​	Get the first page of items where field_offices=miami and sex=male

```
# List of items
curl -X GET https://api.fbi.gov/wanted/v1/list\?page\=1\&field_offices\=miami\&sex\=male -H  "accept: application/json"
```
The returned items also contains an @id property with an url that you can use to poll that single item.

```
# Fetch single item using the @id property url of the item.
curl -X GET https://api.fbi.gov/@wanted-person/99ffed7830acba90db8a7198b9c53afd -H  "accept: application/json" 
```

See the `sample_data` folder for a sample JSON response.

Your task is to create an asynchronous version of the API using websockets.

## 1.1 Websocket API in NodeJS

- Create a websocket server application in NodeJS that serves the same information as the REST API. The application should be an asynchronous version for the actual REST API.
- Should support listing of items and pagination. The search possibility can be ignored.
- Create an integration test for your service.

## 1.2 Docker image

Package your application into a Docker image.

## 1.3 Kubernetes

Write a Kubernetes deployment descriptor for the application and deploy it to a local cluster.

## 1.4 Automation of local development workflow

Some level of automation of the local build/run/integration-test cycle would be nice.

By automation, we mean one or more scripts that simplify command execution, like:

- building the docker image
- deployinging to local cluster
- running integrations tests
- ..etc

This will be helpful in local development, but it could also ease any future setup in a CI/CD environment/pipeline.

# 2 Caching

If we cache the REST service data in our application, we would be able to:
- speed up our service response time
- provide data to our user when the offical REST sevice is unavailable
- provide more fine-grained retrieval options

Add caching to our service and implement a way to fetch a single specific item (specified by the item's 'uid') from the websocket API.

# 3 Event subscriptions

To enable an event-driven use of our API, we want to offer a way to subcribe to events.

Add the option to subscribe to one of the following events in our websocket API:
- a specific existing item was updated
- a new item was added
- a specific item was requested (get notification when someone else fetches a specific item)
- ...or you can define your own event that you want the websocket API to support.

# 4 Persistent caching

What happens if our application crashes or needs a restart?

Make sure the caching of data is persistent so that we do not suffer data loss in case of an application crash or restart.

# 5 Security

The FBI Wanted REST API is public and requires no authentication.

Our websocket version of the API now has additional features that we do not want people to use without being authorized.

How could we secure our new features, while still allowing public access to the inital list items feature?

# 6 Logging

Do we log anything in our application?

What should we log and how?

How would you handle logs when the application is running in the cluster?

# 7 Scaling

Is our application ready to scale?

Do we need to make changes to it in order to run multiple replicas in our Kubernetes cluster?

# 8 Other

Anything else you would suggest to improve the application, developer experience, build pipeline, etc. ?
