#!/usr/bin/env bash

docker build . -t sweet-tech/fbi-wanted-ws && \
docker run -d -p 8080:8080 --name=fbi-wanted-ws sweet-tech/fbi-wanted-ws && \
docker logs fbi-wanted-ws -f