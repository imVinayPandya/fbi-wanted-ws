export interface IConfig {
  env: string | undefined;
  port: string | undefined;
  includeErrorStackTrace: boolean;
  fbiBaseApiUrl: string | undefined;
  db?: {
    user: string | undefined;
    password: string | undefined;
    database: string | undefined;
    host: string | undefined;
    port: string | undefined;
    ssl: boolean;
  },
  redis?: {
    port: string | undefined;
    host: string | undefined;
  },
  aws: {
    logToCloudWatch: boolean;
    accessKeyId: string;
    secretAccessKey: string;
    region: string;
    cloudWatchLogGroupName: string;
    cloudWatchLogStreamName: string;
  }
}

const config: IConfig = {
  env: process.env.NODE_ENV,
  port: process.env.PORT,
  includeErrorStackTrace: process.env.NODE_ENV !== 'production',
  fbiBaseApiUrl: process.env.FBI_API_V1,
  db: {
    user: process.env.DB_USER,
    password: process.env.DB_PASS,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    ssl: process.env.DB_HOST !== 'localhost'
  },
  redis: {
    host: process.env.REDIS_HOST,
    port: process.env.REDIS_PORT
  },
  aws: {
    logToCloudWatch: false,
    accessKeyId: "",
    secretAccessKey: "",
    region: "",
    cloudWatchLogGroupName: "",
    cloudWatchLogStreamName: ""
  }
};

export default config;