import dotenv from 'dotenv-safe';
dotenv.config();

import app from './app';
import config from 'config';
import WebSocket, { createWebSocketStream } from 'ws';
import http, { IncomingMessage } from 'http';
import { Socket } from "net";

import socketAuthentication from './middlewares/socket_auth';
import handleEvent from './ws_event/fbiEvent';
import logger from './utils/logger';

const EVENTS_AUTH = ['person_details'];

//
// Create a WebSocket server completely detached from the HTTP server.
//
const wss = new WebSocket.Server({ clientTracking: false, noServer: true });
//
// Create an HTTP server.
//
const server = http.createServer(app);

server.on('upgrade', function (request: IncomingMessage, socket: Socket, head: Buffer) {
    // @TODO: we can Authorization here for all socket request
    wss.handleUpgrade(request, socket, head, function (ws) {
        wss.emit('connection', ws, request);
    });
});

wss.on('connection', function (ws, request: IncomingMessage & { user: any }) {

    ws.on('message', async function (message: any) {

        let payload = null;

        // parse and validate payload from WS
        try {
            payload = JSON.parse(message.toString());
            if (!payload.type || !payload.data) {
                ws.send('HTTP/1.1 400 bad request');
                ws.close();
                return;
            }
        } catch (error) {
            ws.send('HTTP/1.1 400 Invalid payload format');
            ws.close();
            return;
        }

        if (!payload) {
            ws.send('HTTP/1.1 400 bad request');
            ws.close();
            return;
        }

        logger.info("Payload: "+ JSON.stringify(payload, null, " "));
        // based on Payload action do authorization or just serve the request
        if (EVENTS_AUTH.lastIndexOf(payload?.type) !== -1) {
            logger.debug('Parsing JWT token from request...');

            try {
                await socketAuthentication(request);
                // @TODO: add user to some online user's hash
                // or may be we can maintain status in redis
                // redis.set(userId, ws);

                logger.debug('JWT auth is completed!');
                ws.send("person_details");
            } catch (error) {
                logger.error('Unauthorized request');
                ws.send('HTTP/1.1 401 Unauthorized');
                ws.close();
                return;
            }

        }

        const { type, data } = payload;
        const res = await handleEvent(type, data);
        ws.send(res);
        // const duplexWs = createWebSocketStream(ws, { encoding: 'utf8' });
        // list?.body.pipe(duplexWs);
        // duplexWs.pipe(list?.body)
    });

    ws.on('close', function () {
        // TODO: remove user from redis
    });
});

//
// Start the server.
//
const PORT = config.get('port') || 8080;
server.listen(PORT, function () {
    logger.info(`Listening on http://localhost:${PORT}`);
});